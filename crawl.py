from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import pandas as pd
import requests
import time


url = "https://agmarknet.gov.in/Default.aspx"

connection = requests.get(url)
source = connection.content
soup = BeautifulSoup(source,"html.parser")
#with open("source.html","w") as f:
#    f.write(str(soup))
def extractor(id):
    parent = soup.find("select",{"id":id})
    commodity_group = parent.find_all("option")
    for i in commodity_group:
        com_data = i.get_text()
        yield str(com_data)

driver = webdriver.Firefox(executable_path="/home/mrasr/Documents/driver/geckodriver")
driver.get("https://agmarknet.gov.in/SearchCmmMkt.aspx?Tx_Commodity=137&Tx_State=AN&Tx_District=0&Tx_Market=0&DateFrom=01-Jan-2020&DateTo=26-Jul-2021&Fr_Date=01-Jan-2020&To_Date=26-Jul-2021&Tx_Trend=0&Tx_CommodityHead=Ajwan&Tx_StateHead=Andaman+and+Nicobar&Tx_DistrictHead=--Select--&Tx_MarketHead=--Select--")
driver.execute_script("window.scrollTo(0,250)")
time.sleep(8)

def crawl_page(page_data):
    try:

        for i in range(50):
            crawl_soup = BeautifulSoup(page_data,"html.parser")
            crawl_parent = crawl_soup.find("table",class_="tableagmark_new")
            district_quries = crawl_parent.find("span",{"id":"cphBody_GridPriceData_Labdistrict_name_%d" % (i)})
            market_quries =  crawl_parent.find("span",{"id":"cphBody_GridPriceData_LabdMarketName_%d" % (i)})
            commodity_quries = crawl_parent.find("span",{"id":"cphBody_GridPriceData_Labcomm_name_%d" % (i)})
            variety_quries = crawl_parent.find("span",{"id":"cphBody_GridPriceData_LabdVariety_%d" % (i)})
            min_price_quries = crawl_parent.find("span",{"id":"cphBody_GridPriceData_LabMinPrice_%d" % (i)})
            max_price_quries = crawl_parent.find("span",{"id":"cphBody_GridPriceData_Labmaxpric_%d" % (i)})
            modal_price_quries = crawl_parent.find("span",{"id":"cphBody_GridPriceData_LabModalpric_%d" % (i)})
            report_date_quries =  crawl_parent.find("span",{"id":"cphBody_GridPriceData_LabReportedDate_%d" % (i)})
            district = district_quries.get_text()
            market = market_quries.get_text()
            commodities = commodity_quries.get_text()
            variety = variety_quries.get_text()
            min_price = min_price_quries.get_text()
            max_price = max_price_quries.get_text()
            model_price = modal_price_quries.get_text()
            report_date = report_date_quries.get_text()
            district_group.append(district)
            market_group.append(market)
            commodity_group.append(commodities)
            variety_group.append(variety)
            min_price_group.append(min_price)
            max_price_group.append(max_price)
            model_price_group.append(model_price)
            report_date_group.append(report_date)
    except :
        pass
y = 0#just remove it to scrape all state data 
for state_selector in extractor("ddlState"):
    district_group = []
    market_group = []
    commodity_group = []
    variety_group = []
    min_price_group = []
    max_price_group = []
    model_price_group = []
    report_date_group = []
    if state_selector == "--Select--":
        continue
    time.sleep(2)
    state = Select(driver.find_element_by_id("ddlState"))
    state.select_by_visible_text(state_selector)
    time.sleep(2)
    x = 0#just remove it scrape all data
    y+= 1#just remove it scrape all data
    if y == 5:#just remove it scrape all data
        break
    for commodity_selector in extractor("ddlCommodity"):
        if commodity_selector == "--Select--":
            continue
        time.sleep(2)
        commodity = Select(driver.find_element_by_id("ddlCommodity"))
        commodity.select_by_visible_text(commodity_selector)
        time.sleep(1)
        go = driver.find_elements_by_xpath("//*[@id='btnGo']")[0].click()
        time.sleep(5)
        driver.execute_script("window.scrollTo(0,400)")
        page_data_source = driver.page_source
        crawl_page(page_data_source)

        try:
            next_button = driver.find_elements_by_xpath("/html/body/form/div[3]/div[6]/div[6]/div[1]/div[2]/div[3]/div/table/tbody/tr[52]/td/table/tbody/tr/td[1]/input")[0]
            next_button.click()
            time.sleep(7)
            driver.execute_script("window.scrollTo(0,400)")
            next_page_content = driver.page_source
            crawl_page(next_page_content)

            def next_page_scrap():

                next_button_next = driver.find_elements_by_xpath("/html/body/form/div[3]/div[6]/div[6]/div[1]/div[2]/div[3]/div/table/tbody/tr[52]/td/table/tbody/tr/td[3]/input")[0]
                next_button_next.click()
                time.sleep(7)
                next_page_content_next = driver.page_source
                crawl_page(next_page_content_next)
        
            while True:
                next_page_scrap()

        except IndexError as error:
            pass
        x+=1         #just remove it to scrap all the data
        if x == 5:#just remove it to scrap all the data
            break #just remove it to scrap all the data

    
    

    state_data=dict()   
    state_data["District"] = district_group 
    state_data["Market"] = market_group
    state_data["Commodity"] = commodity_group
    state_data["Variety"] = variety_group
    state_data["Min-Price"] = min_price_group
    state_data["Max-Price"] = max_price_group
    state_data["Modal-Price"] = model_price_group
    state_data["Report_Date"] = report_date_group

    excel = pd.DataFrame(state_data)
    filename = f"{state_selector}.csv"   
    excel.to_csv(filename)
    
    

  